'''
Created on 16 Oct 2015

@author: bernamp
'''

import __main__
from os.path import os
import shutil
import urllib2
import webbrowser
from zipfile import ZipFile

from ec.europa.eu.actions.DBConfigs import callMysql
from ec.europa.eu.actions.INI import INI
from ec.europa.eu.actions.XMLconfigs import XMLconfigs
from ec.europa.eu.actions.certificates import certificates
from ec.europa.eu.utilities.ConfigParserUtils import Helper
from ec.europa.eu.utilities.logs import Logging
from ec.europa.eu.utilities.utils import utl
import subprocess


config = {}
resourceFolder = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'resources')


#Check if software version is installed and at the correct version
#if not install it according to the version    
def installationRequiredSoftware():
    print "TODO"
    #http://stackoverflow.com/questions/18357048/python-with-yum-api-installing-package

def newInstallationStartup():
    if os.path.exists(config["target_folder"]):
        shutil.rmtree(config["target_folder"])
    mysql.removeEpsosDB()
    if not os.path.exists(config["target_folder"]):
        os.mkdir(config["target_folder"])
        
def installOpenNCP():
    mysql.createOpenNCPDB()
    
    liferay_path = os.path.join(config['liferay_path'], "liferay_openNCP")    
    #===========================================================================
    # if os.path.exists(liferay_path):
    #     print 'Remove existing installation of openNCP'
    #     shutil.rmtree(liferay_path)
    #===========================================================================
    if config['skip_liferay_download'].lower() == 'no':
        if not os.path.exists(config['liferay_path']):
            os.makedirs(config['liferay_path'])
        print '========> Download liferay. It\'s a large file. (300MB)' 
        liferay_url = urllib2.urlopen(os.path.join('http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/6.2.0%20GA1/liferay-portal-tomcat-6.2.0-ce-ga1-20131101192857659.zip?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Flportal%2Ffiles%2FLiferay%2520Portal%2F6.2.0%2520GA1%2F&ts=1402987429&use_mirror=iweb'))
        with open(os.path.join(config['liferay_path'], "liferay.zip" ), 'w') as liferay_zip:
            liferay_zip.write(liferay_url.read()) 
    
    with ZipFile(os.path.join(config['liferay_path'], "liferay.zip" )) as liferay_zip:
        liferay_zip.extractall(config['liferay_path'])
    os.rename(os.path.join(config['liferay_path'], 'liferay-portal-6.2.0-ce-ga1'), liferay_path)
    xml.openNCPServerxml(os.path.join(liferay_path, 'tomcat-7.0.42', 'conf', 'server.xml'))
    
    createProperties.portal_ext(liferay_path)
    createProperties.tsam_properties_update()
        
    shutil.copy(os.path.join(config['resourcesFolder'] , 'context_template.xml'), os.path.join(liferay_path, 'tomcat-7.0.42', 'conf'))
    
    print 'Starting liferay Tomcat at %s' % os.path.join(liferay_path, 'tomcat-7.0.42', 'bin', 'startup.sh')
    subprocess.check_call(['chmod', '-R', '777', liferay_path])
    subprocess.check_call([os.path.join(liferay_path, 'tomcat-7.0.42', 'bin', 'startup.sh')])
    print "You can check the status of Tomcat installation with this command \n%s" % "tail -f /home/berna/liferay/liferay_openNCP/tomcat-7.0.42/logs/catalina.out"
    #subprocess.call(os.liferay_path.join(liferay_path, 'tomcat-7.0.42', 'bin', 'startup.sh'))
    print 'opening page : http://%s:%s' %(config['databasehost'], config['openncp_http'])
    print "If you don't have a gui, you need to do it manually"
    raw_input("Press [ENTER] tomcat is started")
    webbrowser.open('http://%s:%s' %(config['databasehost'], config['openncp_http']))   
    raw_input("Press [ENTER] when the configuration of liferay is done")
    
    utl.installApps("openncp-portal", 'war', "openncp-portal", os.path.join(joinup_path, 'openncp-portal'), os.path.join(liferay_path, 'tomcat-7.0.42', 'webapps'))
    
    
if __main__:
    INI(config, resourceFolder).parse()
    config['EPSOS_PROPS_PATH'] = os.path.join(config["target_folder"], "epsos-configuration")
    os.environ['EPSOS_PROPS_PATH'] = config['EPSOS_PROPS_PATH']
    mysql = callMysql(config)
    
    createProperties = Helper(config)
    
    newInstallationStartup()
    xml =  XMLconfigs(config)
    utl = utl(config, xml)
    joinup_path = 'https://joinup.ec.europa.eu/nexus/content/repositories/releases/eu/europa/ec/joinup/ecc'
     
    
    # TODO check if user is owner of the folders 
    #utl.isSudo()
    print "######## OpenNCP Installation tool ###########"
    Logging(True).machineRequirements()
     
    installationRequiredSoftware()
 
    ######## 1. Setup application server
    xml.tomcatConfiguration()
     
    ######### 2. Adjust configuration parameters
    ZipFile(os.path.join(resourceFolder,'epsos-configuration.zip')).extractall(config["target_folder"])
     
    ######### 2.1 Configuration Manager Database
    mysql.createEpsosDB(xml)
 
     
    ######### 2.3 Creation of certificate
    c = certificates(config)
    c.createEnvironment()
    c.createCertViaScripts()
 
    ######### 2.2 NCP First-Time Configuration Utility
    #os.chdir(os.environ["EPSOS_PROPS_PATH"])
    #subprocess.call(['java', '-jar', 'OpenNCPSetupUtil.jar'])
    # Temporary fix for dependency missing
    mysql.loadProperties(c.certificates)
    xml.addC3PO()
 
    ######### 3. Install and setup components
    ######### 3.1 OpenNCP artifacts
 
    ######### 3.2 TRC-STS
    utl.installApps("epsos-trc-sts", 'war', "TRC-STS", os.path.join(joinup_path, "epsos-trc-sts"), config["tomcat_webapps"])
    #configure the jdbc/ConfMgr data source in your Tomcat conf/context.xml to connect to your epSOS properties database.
    #After that, you can check the property "secman.sts.url" at your NCP properties database to see if it matches the installed application URL.
     
    ######### 3.3 TSL (Trusted Service List)
 
    os.mkdir(os.path.join(config['target_folder'],'tsl-sync'))
    utl.installApps("epsos-tsl-sync", 'jar', "TSL-SYNC", os.path.join(joinup_path, 'epsos-tslutils', 'epsos-tsl-sync'), os.path.join(config['target_folder'], "tsl-sync"))
 
    ######### 3.4 TSAM-Sync
    ZipFile(os.path.join(resourceFolder, 'tsam-sync-folder.zip')).extractall(os.path.join(config["target_folder"], "tsam-sync"))
    #os.mkdir(os.joinup_path.join(config['target_folder'],'tsam-sync'))
    utl.installApps("epsos-tsam-sync", 'jar', 'tsam-sync', os.path.join(joinup_path, "epsos-tsam-sync"), os.path.join(config['target_folder'], "tsam-sync"))
 
    #Edit log4j reference in sync.sh to:
    #$TSAM_DIR should be a script variable that references tsam-sync folder
    #-Dlog4j.configuration=file:///$TSAM_DIR/conf/log4j.xml
    #sync.sh -jar should not point to a concrete version 
 
    ######### 3.5 Transformation Synchronization Access Manager (TSAM)
 
    print "TODO remove TSAM.properties from epsos_path"
    createProperties.tsam_properties()
 
    ######### 3.6 Transformation Manager (TM)
 
    print "Nothing to do here..."
    print "TODO Create tm.properties on the fly"
 
    ######### 3.7 Automatic Data Collector (eADC)
 
    mysql.createEADC()
    for d in ['db', 'demo', 'doc']:
        if os.path.exists(d):
            shutil.rmtree(os.path.join(config['EPSOS_PROPS_PATH'], "EADC_resources", d))

    ######### 3.8 Audit Repository (OpenATNA)
    atna_home = os.path.join(config["EPSOS_PROPS_PATH"], 'ATNA_resources')
    shutil.copy(c.certificates['SP_KEYSTORE_PATH'], os.path.join(atna_home, 'certs'))
    shutil.copy(c.certificates['SC_KEYSTORE_PATH'], os.path.join(atna_home, 'certs'))
    xml.configArrConnections(c.certificates, atna_home)
    mysql.openATNA()
    createProperties.atna_properties(atna_home)

    utl.installApps("openatna-web", 'war', "openatna-web", os.path.join(joinup_path, 'epsos-openatna', 'openatna-web'), config["tomcat_webapps"])

    ######### 3.9 Server Side (NCP-A)

    utl.installApps("epsos-ws-server", 'war', "epsos-ws-server", os.path.join(joinup_path, 'epsos-protocol-terminators', 'epsos-ncp-server', "epsos-ws-server"), config["tomcat_webapps"])

    ######### 3.10 Client Side (NCP-B)
    utl.installApps("epsos-client-connector", 'war', "epsos-client-connector", os.path.join(joinup_path, 'epsos-protocol-terminators', 'epsos-ncp-client', "epsos-client-connector"), config["tomcat_webapps"])

    ######### 3.11 OpenNCP Portal or epSOS-Web
    if config["portal"] == 'OpenNCP':
        installOpenNCP()
    elif config["portal"] == 'epSOS-Web':
        utl.installApps("epSOS-Web", 'war', "epSOS-Web", os.path.join(joinup_path, 'epSOS-Web'), config["tomcat_webapps"])
    else:
        print "Unknown %s  or unspecified portal. Skipping" % config["portal"]

        
    #########3.11.3 CDA Display Tool (TSAM-Exporter)
    mysql.tsam_exporter()
    tsam_home = os.path.join(config["target_folder"], 'tsam-exporter')
    os.mkdir(tsam_home)
    utl.installApps("epsos-tsamexporter", 'jar', "tsam-exporter", os.path.join(joinup_path, 'epsos-cdadisplaytool', 'epsos-tsamexporter'), tsam_home)
    shutil.copy(os.path.join(resourceFolder, 'logging.properties'), tsam_home)
    shutil.copy(os.path.join(resourceFolder, 'run.sh'), tsam_home)
    
    createProperties.tsam_exp_properties(tsam_home)
            
    #subprocess.check_call(os.joinup_path.join(tsam_home, "run.sh"))

    ######### 4. Database Logging
    # FINAL CHECKS

    print "DONE"