'''
Created on Oct 26, 2015

@author: berna
'''
from os.path import os
import shutil
import subprocess
import zipfile


class certificates(object):
    '''
    classdocs
    '''
    
    def __init__(self, config):
        self.config = config
        self.env = config['certificate_environment']
        self.key = '%s-ca.key' % config['country_code']
        self.exp = config['certificate_expiration']
        self.pwd = config['certificate_pwd']
        self.pem = os.path.join('ROOT','%s-ca.pem' % self.config['country_code'])
        self.country_keystore_container = []
        self.keyToolExports = []
        self.certificates = {}
        
    def createEnvironment(self):
        os.mkdir(os.path.join(self.config["EPSOS_PROPS_PATH"],'cert'))
        os.mkdir(os.path.join(os.path.join(self.config["EPSOS_PROPS_PATH"],'cert', self.env)))
        os.chdir(os.path.join(self.config["EPSOS_PROPS_PATH"],'cert', self.env))
        zipfile.ZipFile(os.path.join(self.config['resourcesFolder'],'epSOS_config.zip')).extractall(os.path.join(self.config["EPSOS_PROPS_PATH"],'cert', self.env))
    
    def createCertViaScripts(self):
        self.caCert()
        self.selfCert() 
    ######### 2.3 Creation of certificates
    def caCert(self):
        os.mkdir('ROOT')
        keypath = os.path.join('ROOT','%s-ca.key' % self.config['country_code'])
        self.openssl('genrsa',  '-des3', '-out', keypath, '-passout', 'pass:%s' % self.pwd, '4096' )
        print 'Key created at %s' % keypath    
        self.openssl('req',  '-new', '-x509', '-days', self.exp, '-key', keypath, '-out', self.pem,  '-passin', 'pass:%s' % self.pwd, '-subj', self.createSubject())
        print 'Certificate created at %s' % self.pem 
        
    def selfCert(self):
        caSerial = os.path.join('private', '%s-ca.srl' % self.config['country_code'])

        print 'Creating certificate request for %s' %  self.config['country_code']

        print "Deleting keystore"
        for d in ['pem', 'private', 'test_requests', 'keystore']:
            if os.path.exists(d):
                shutil.rmtree(d)
            os.mkdir(d)
        if os.path.exists(os.path.join('ROOT', caSerial)):
            os.remove(os.path.join('ROOT', caSerial))
        
        caKey = os.path.join('ROOT', self.key)
        print "*** NCP Signature ***"
        self.part1('sign', 'sig', 'NCPsignature', caSerial, caKey)
        self.part2('sign', 'signature', 'signature', 'sig')
       
        print "*** Service Consumer (NCP Client) ***"
        self.part1('sc', 'sc', 'ServiceConsumer', caSerial, caKey)
        self.part2('sc', 'service-consumer', 'sc', 'sc')
       
        print "*** Service Provider (NCP Server) ***"
        self.part1('sp', 'sp', 'ServiceProvider', caSerial, caKey)
        self.part2('sp', 'service-provider', 'sp', 'sp')
        
        print "*** VPN Server ***"
        self.part1('vpn-server', 'vpn-server', 'VPNserver', caSerial, caKey)
        
        print "*** VPN Client ***"
        self.part1('vpn-server', 'vpn-client', 'VPNclient', caSerial, caKey)
        
        print "*** OCSP Responder ***"
        self.part1('npc-ocsp', 'npc-ocsp', 'OCSPresponder', caSerial, caKey)
        self.part2('npc-ocsp', 'ocsp', 'ocsp', 'npc-ocsp')
        
        print "*** Importing a new Trusted Certificate"
        self.importKeyStore()
        
    def importKeyStore(self):
        print "*** Importing CA certificates into the keystore"
        alias = "%s.ca.epsos.%s.%s" % (self.config['certificate_environment'], self.config['certificate_organization'],self.config['country_code'])
        for c in self.country_keystore_container:
            self.keytool('-importcert', '-alias', alias, '-file', self.pem, '-keystore', c, '-storepass', self.config['certificate_pwd_ks']) 
        
        country_truststore = 'keystore/%s-truststore.jks' % self.config['country_code']
        if self.config['initialtruststore'] != '':
            shutil.copy(self.config['initialTrustStore'], country_truststore)
        else:
            self.keytool('-importcert', '-alias', alias, '-file', self.pem, '-keystore', country_truststore, '-storepass', self.config['certificate_pwd_ks']) 
    
        for k in self.keyToolExports:
            self.keytool('-importcert', '-alias', k[0], '-file', k[1], '-keystore', country_truststore, '-storepass', self.config['certificate_pwd_ts'])
        
        self.certificates['TRUSTSTORE_PATH'] = os.path.abspath(country_truststore)
        self.certificates['TRUSTSTORE_PASSWORD'] = self.config['certificate_pwd_ks']
        
            
    def part1(self, name, short_name, conf_name, caSerial, caKey):
        ncp_key = os.path.join('private', '%s-ncp-%s.key' % (self.config['country_code'], name))
        ncp_csr = os.path.join('test_requests', '%s-ncp-%s.csr' % (self.config['country_code'], name))
        ncp_self_sign_pem = os.path.join('pem', '%s-ncp-%s-self-sign.pem' % (self.config['country_code'], short_name))
        conf_r = 'conf/config-%s-csr' % conf_name
        conf_t = 'conf/config-%s-crt' % conf_name
        
        self.openssl('req',  '-new', '-sha512', '-nodes', '-newkey', 'rsa:2048', '-keyout', ncp_key,  '-out', ncp_csr, '-config', conf_r, '-text', '-utf8' )
        self.openssl('x509',  '-req', '-days', self.exp, '-in', ncp_csr, '-CAcreateserial', '-CAserial', caSerial, '-CA', self.pem, '-CAkey', caKey, '-out', ncp_self_sign_pem, '-extfile', conf_t, '-passin', 'pass:%s' % self.config['certificate_pwd_ca'], '-sha512')
        
    def part2(self, name, long_name, longAndShort,short_name):
        ncp_key = os.path.join('private', '%s-ncp-%s.key' % (self.config['country_code'], name))#sign, sc, sp
        ncp_country_p12 = os.path.join('keystore', 'ncp-%s-%s.p12' % (self.config['country_code'], name))
        country_keystore_jks = os.path.join('keystore', '%s-%s-keystore.jks' % (self.config['country_code'], long_name)) #signature, service-consumer, service-consumer
        self.country_keystore_container.append(country_keystore_jks)
        ncp_alias = '%s.ncp-%s.epsos.%s.%s' %(self.config['certificate_environment'].lower(), longAndShort,self.config['certificate_organization'].lower().replace(" ", "-"),self.config['country_code'].lower())#signature, sc, sp
        ncp_self_sign_pem = os.path.join('pem', '%s-ncp-%s-self-sign.pem' % (self.config['country_code'], short_name))
        self.keyToolExports.append((ncp_alias, ncp_self_sign_pem))
        
        self.openssl('pkcs12', '-export', '-in', ncp_self_sign_pem, '-inkey', ncp_key, '-password', 'pass:%s' % self.config['certificate_pwd_ks'], '-out', ncp_country_p12)
        self.keytool('-importkeystore', '-srckeystore', ncp_country_p12, '-destkeystore', country_keystore_jks, '-srcstoretype', 'pkcs12', '-deststorepass', self.config['certificate_pwd_ks'], '-srcstorepass', self.config['certificate_pwd_ks'])
        self.keytool('-changealias', '-v', '-alias', '1', '-destalias', ncp_alias, '-keystore', country_keystore_jks, '-storepass', self.config['certificate_pwd_ks'])
        
        self.certificates['%s_KEYSTORE_PATH' % short_name.upper()] = os.path.abspath(country_keystore_jks)
        self.certificates['%s_KEYSTORE_PASSWORD' % short_name.upper()] = self.config['certificate_pwd_ks']
        self.certificates['%s_PRIVATEKEY_ALIAS' % short_name.upper()] = ncp_alias
        self.certificates['%s_PRIVATEKEY_PASSWORD' % short_name.upper()] = self.config['certificate_pwd_ks']
        
    def openssl(self, *args):
        #self.command('openssl', args)
        cmdline = ['openssl'] + list(args)
        print ' '.join(cmdline)
        subprocess.check_call(cmdline)        
   
    def keytool(self, *args):
        cmdline = ['keytool', '-noprompt'] + list(args)
        subprocess.check_call(cmdline)
        
    def createSubject(self):
        return "/%s=%s/%s=%s/%s=%s/%s=%s/%s=%s/%s=%s/%s=%s/" %(
                                                      'C', self.config['country_code'].upper(),
                                                      'ST', self.config['certificate_state'].upper(),
                                                      'L', self.config['certificate_city'].upper(),
                                                      'O', self.config['certificate_organization'].upper(),
                                                      'OU', self.config['certificate_department'].upper(),
                                                      'CN', self.config['certificate_common_name'].upper(),
                                                      'emailAddress', self.config['certificate_email']                                                  
                                                      )
        
    
