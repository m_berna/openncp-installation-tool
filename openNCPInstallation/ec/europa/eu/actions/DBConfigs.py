'''
Created on Oct 23, 2015

@author: berna
'''
from os.path import os
import sys

import mysql.connector as cnx


class callMysql(object):
    '''
    classdocs
    '''

    def __init__(self, config):
        self.config = config

    ######### 2.1 Configuration Manager Database
    def createEpsosDB(self, xml):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                result = cursor.execute("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '%s'" % self.config["database_name"])
                if result == (0 or None):
                    xml.configmanagerHibernate()
                    cursor.execute("create database %s" % self.config["database_name"])
                else:
                    # Exists!!
                    print "DATABASE ALREADY EXISTS MIGRATE FUNCTION TO BE IMPLEMENTED!!!"
                    sys.exit(1)
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
                
    def removeEpsosDB(self):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                cursor.execute("DROP DATABASE IF EXISTS %s" % self.config["database_name"])
                cursor.execute("DROP DATABASE IF EXISTS openatna")
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
                
    def createEADC(self):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                #TODO remove external file!!
                create = open(os.path.join(self.config["EPSOS_PROPS_PATH"], "EADC_resources", "db", "CREATE_EADC.sql"), 'r').read().split(';')
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                cursor.execute("use %s" % self.config['database_name'])
                cursor.execute(create[0])
                cursor.execute(create[1])
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
    def openATNA(self):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                cursor.execute("create database if not exists openatna default character set = utf8")
                #cursor.execute("create user '%s'@'localhost' identified by '%s'" %(self.config['atna_user'], self.config['atna_password']))
                cursor.execute("grant all on %s.* to 'openatna'@'localhost' identified by '%s'"%(self.config['atna_user'], self.config['atna_password']))
                #cursor.execute("alter messages change messageContent messageContent blob null")
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
    
    def createOpenNCPDB(self):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                with open (os.path.join(self.config['resourcesFolder'], 'liferay_create', 'create-mysql.sql'), "r") as create:
                    for stmt in create.read().split(';'):
                        try:
                            a = stmt.strip('\n')
                            if len(a) is not 0:
                                cursor.execute(a)
                        except cnx.Error as e:
                            print("Statement failure: %s" % a)
                            print("Error: {}".format(e))

                con.commit()
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
                
    def tsam_exporter(self):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'])
                cursor = con.cursor(buffered=True)
                cursor.execute("create database if not exists ltrdb default character set = utf8")
                con.commit()
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
                
    
    def loadProperties(self, cert):
        if  self.config["db_type"].upper() == 'MYSQL':
            try:
                con = cnx.connect(host=self.config['databasehost'], port=int(self.config['port']), user=self.config['username'], passwd=self.config['password'], database=self.config['database_name'])
                cursor = con.cursor(buffered=True)
                cursor.execute("create table epsosDB.property (name varchar(255), `value` varchar(255))")
                cursor.execute("insert into property values ('%s', '%s')" % ('ncp.country', self.config['certificate_state']))
                cursor.execute("insert into property values ('%s', '%s')" % ('ncp.email', self.config['certificate_email']))
                cursor.execute("insert into property values ('%s', '%s')" % ('COUNTRY_CODE', self.config['country_code']))
                cursor.execute("insert into property values ('%s', '%s')" % ('COUNTRY_NAME', self.config['certificate_state']))
                cursor.execute("insert into property values ('%s', '%s')" % ('LANGUAGE_CODE', self.config['language']))
                cursor.execute("insert into property values ('%s', '%s')" % ('HOME_COMM_ID', self.config['home_comm_id']))
                cursor.execute("insert into property values ('%s', '%s')" % ('COUNTRY_PRINCIPAL_SUBDIVISION', self.config['country_principal_subdivision']))
                cursor.execute("insert into property values ('%s', '%s')" % ('TRUSTSTORE_PATH', cert['TRUSTSTORE_PATH']))
                cursor.execute("insert into property values ('%s', '%s')" % ('TRUSTSTORE_PASSWORD', cert['TRUSTSTORE_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SP_KEYSTORE_PATH', cert['SP_KEYSTORE_PATH']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SP_KEYSTORE_PASSWORD', cert['SP_KEYSTORE_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SP_PRIVATEKEY_ALIAS', cert['SP_PRIVATEKEY_ALIAS']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SP_PRIVATEKEY_PASSWORD', cert['SP_PRIVATEKEY_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SC_KEYSTORE_PATH', cert['SC_KEYSTORE_PATH']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SC_KEYSTORE_PASSWORD', cert['SC_KEYSTORE_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SC_PRIVATEKEY_ALIAS', cert['SC_PRIVATEKEY_ALIAS']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SC_PRIVATEKEY_PASSWORD', cert['SC_PRIVATEKEY_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('NCP_SIG_KEYSTORE_PATH', cert['SIG_KEYSTORE_PATH']))
                cursor.execute("insert into property values ('%s', '%s')" % ('NCP_SIG_KEYSTORE_PASSWORD', cert['SIG_KEYSTORE_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('NCP_SIG_PRIVATEKEY_ALIAS', cert['SIG_PRIVATEKEY_ALIAS']))
                cursor.execute("insert into property values ('%s', '%s')" % ('NCP_SIG_PRIVATEKEY_PASSWORD', cert['SIG_PRIVATEKEY_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('javax.net.ssl.keyStorePassword', cert['SIG_KEYSTORE_PASSWORD']))
                cursor.execute("insert into property values ('%s', '%s')" % ('secman.sts.url', self.config['secman_sts_url']))
                cursor.execute("insert into property values ('%s', '%s')" % ('automated.validation', self.config['automated_validation']))
                cursor.execute("insert into property values ('%s', '%s')" % ('SERVER_IP', self.config['server_ip']))
                cursor.execute("insert into property values ('%s', '%s')" % ('audit.repository.port', self.config['audit.repository.port']))
                cursor.execute("insert into property values ('%s', '%s')" % ('audit.repository.url', self.config['audit.repository.url']))
                cursor.execute("insert into property values ('%s', '%s')" % ('audit.time.to.try', self.config['audit.time.to.try']))
                cursor.execute("insert into property values ('%s', '%s')" % ('auditrep.forcewrite', self.config['auditrep.forcewrite']))
                cursor.execute("insert into property values ('%s', '%s')" % ('WRITE_TEST_AUDITS', self.config['write_test_audits']))
                # TODO automate the following
                cursor.execute("insert into property values ('%s', '%s')" % ('TEST_AUDITS_PATH', self.config['test_audits_path']))
                cursor.execute("insert into property values ('%s', '%s')" % ('scheduled.time.between.failed.logs.handling.minutes', self.config['scheduled.time.between.failed.logs.handling.minutes']))
                cursor.execute("insert into property values ('%s', '%s')" % ('certificates.storepath', self.config['certificates.storepath']))
                cursor.execute("insert into property values ('%s', '%s')" % ('ncp.countries', self.config['ncp.countries']))

                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.at', self.config['tsl.location.at']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.ch', self.config['tsl.location.ch']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.cz', self.config['tsl.location.cz']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.de', self.config['tsl.location.de']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.dk', self.config['tsl.location.dk']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.ee', self.config['tsl.location.ee']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.es', self.config['tsl.location.es']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.fi', self.config['tsl.location.fi']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.fr', self.config['tsl.location.fr']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.gr', self.config['tsl.location.gr']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.hr', self.config['tsl.location.hr']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.hu', self.config['tsl.location.hu']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.ih', self.config['tsl.location.ih']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.it', self.config['tsl.location.it']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.mt', self.config['tsl.location.mt']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.pt', self.config['tsl.location.pt']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.se', self.config['tsl.location.se']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.si', self.config['tsl.location.si']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.sk', self.config['tsl.location.sk']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.tr', self.config['tsl.location.tr']))
                cursor.execute("insert into property values ('%s', '%s')" % ('tsl.location.lu', self.config['tsl.location.lu']))
                con.commit()
                cursor.close()
                con.close()
            except cnx.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exit(1)
        