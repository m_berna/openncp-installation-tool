'''
Created on Oct 23, 2015

@author: berna
'''
import ConfigParser
from os.path import os
import sys


class INI(object):
    '''
    Only retrieve ini file
    '''


    def __init__(self, config, path):
        self.path = path
        self.config = config
        self.config['resourcesFolder'] = self.path
        
    def parse(self):
        print "Reading configuration"
        Config = ConfigParser.ConfigParser()
        if not os.path.exists(os.path.join(self.path, 'config.ini')):
            print "config.ini not found at path %s" %self.path
            sys.exit(-1) 
        Config.read(os.path.join(self.path, 'config.ini'))
        
        for section in Config.sections():
            options = Config.options(section)
            for option in options:
                try:
                    self.config[option] = Config.get(section, option)
                    if self.config[option] == -1:
                        print("skip: %s" % option)
                except:
                    print("exception on %s!" % option)
                    self.config[option] = None
        
        if self.config['db_type'].upper() == 'MYSQL':
            # Hibeernate
            self.config["hibernate.dialect"] =  'org.hibernate.dialect.MySQLDialect'
            self.config["hibernate.connection.driver_class"] = 'com.mysql.jdbc.Driver'
            self.config["hibernate.connection.username"] = self.config['username']
            self.config["hibernate.connection.password"] =  self.config['password']
            self.config["hibernate.connection.url"] = "jdbc:mysql://%s:%s/%s?useUnicode=true&amp;characterEncoding=UTF-8&amp;useFastDateParsing=false" %(self.config['databasehost'], self.config['port'], self.config['database_name'])
            
            #Tomcat
            self.config["driverClassName"] = self.config['hibernate.connection.driver_class']
            self.config["url"] = "jdbc:mysql://%s:%s/%s?zeroDateTimeBehavior=convertToNull" %(self.config['databasehost'], self.config['port'], self.config['database_name'])
        
         #===========================================================================
        # elif config["type"].upper() == 'SQLSERVER':
        #     config["dialect"] = 'org.hibernate.dialect.MySQLDialect'
        #     config["driver"] = 'com.mysql.jdbc.Driver'
        # elif config["type"].upper() ==  'ORACLE':
        #     config["dialect"] = 'org.hibernate.dialect.OracleDialect'
        #     config["driver"] = 'com.mysql.jdbc.Driver'
        # elif config["type"].upper() ==  'POSTGRES':
        #     config["dialect"] = 'org.hibernate.dialect.PostgreSQLDialect'
        #     config["driver"] = 'com.mysql.jdbc.Driver'
        #===========================================================================
        else:
            print "Error: Database %s not implemented" % self.config["type"]
            sys.exit(1) 
