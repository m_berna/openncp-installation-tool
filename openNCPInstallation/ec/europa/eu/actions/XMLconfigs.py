'''
Created on Oct 23, 2015

@author: berna
'''
from _elementtree import Element
from os.path import os
import sys
import urllib2
from xml.dom import minidom

import xml.etree.ElementTree as ET


class XMLconfigs(object):
    '''
    classdocs
    '''

    def __init__(self, config):
        self.config = config
        self.tempC3PO = None;
        self.doctype = """<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE hibernate-configuration SYSTEM "http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd">"""
        
    def tomcatConfiguration(self):
        tree = ET.parse(os.path.join(self.config['resourcesFolder'] , 'context_template.xml'))
        for root in tree.getroot().iter('Resource'):
            for attr in root.attrib:
                if attr in self.config.keys():
                    root.attrib[attr] = self.config[attr]
    
        tree.write(os.path.join(self.config["tomcat_context_folder"], "context.xml"), encoding='utf-8', xml_declaration=True)

    def configmanagerHibernate(self):
        tree = ET.parse(os.path.join(self.config['resourcesFolder'] , 'configmanager.hibernate_template.xml'))
        root = tree.find('session-factory')
        for prop in root:
            val = prop.get('name', default=None)
            if val in self.config.keys():
                prop.text = self.config[val]
        
        with open(os.path.join(self.config["EPSOS_PROPS_PATH"], "configmanager.hibernate.xml"), 'w') as f:
            f.write(self.doctype)
            tree.write(f, 'utf-8')
        #tree.write(os.path.join(self.config["EPSOS_PROPS_PATH"], "configmanager.hibernate.xml"), xml_declaration=True) 

    def addC3PO(self):
        tree = ET.parse(os.path.join(self.config["EPSOS_PROPS_PATH"], 'configmanager.hibernate.xml'))

        root = tree.find('session-factory')
        for t in [('hibernate.connection.provider_class', 'org.hibernate.connection.C3P0ConnectionProvider'), ('hibernate.c3p0.min_size', '1'), ('hibernate.c3p0.max_size', '50'), ('hibernate.c3p0.timeout', '7200'), ('hibernate.c3p0.max_statements', '50'), ('hibernate.c3p0.validate', 'true'), ('hibernate.c3p0.idle_test_period', '30'), ('hibernate.c3p0.preferredTestQuery', 'SELECT 1 FROM DUAL')]:
            el = Element('property', {'name': t[0]})
            el.text = t[1]
            root.append(el)

        with open(os.path.join(self.config["EPSOS_PROPS_PATH"], "configmanager.hibernate.xml"), 'w') as f:
            f.write(self.doctype)
            tree.write(f, 'utf-8')
            
    def getMavenVersion(self, url, version):
        try:
            tree = ET.parse(urllib2.urlopen(url, timeout = 5))
            v = tree.find('versioning/%s' % version)
            if v != None:
                # Release always present!!!
                return v.text
        except urllib2.URLError as e:
            print "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit(1)
        
    def configArrConnections(self, cert, atna_home):
        tree = ET.parse(os.path.join(atna_home, 'ArrConnections.xml'))
        tree.find('SecureConnection/HostName').text = self.config['audit.repository.url']
        tree.find('SecureConnection/Port').text = self.config['audit.repository.port']
        tree.find('SecureConnection/KeyStore').text = os.path.join(atna_home, 'certs', os.path.basename(cert['SP_KEYSTORE_PATH']))
        tree.find('SecureConnection/KeyPass').text = cert['SP_PRIVATEKEY_PASSWORD']
        tree.find('SecureConnection/TrustStore').text = os.path.join(atna_home, 'certs', os.path.basename(cert['SC_KEYSTORE_PASSWORD']))
        tree.find('SecureConnection/TrustPass').text = cert['SC_KEYSTORE_PASSWORD']

        with open(os.path.join(atna_home, "ArrConnections.xml"), 'w') as f:
            f.write(self.doctype)
            tree.write(f, 'utf-8')
            
    def openNCPServerxml(self, server):
        with open (os.path.join(self.config['resourcesFolder'] , 'server_openncp_template.xml'), "r") as template:
            temp = template.read()
            temp = temp.replace('%%openncp_shutdown%%',self.config['openncp_shutdown'])
            temp = temp.replace('%%openncp_http%%',self.config['openncp_http'])
            temp = temp.replace('%%openncp_ajp%%',self.config['openncp_ajp'])

        with open(os.path.join(server), 'w') as f:
            f.write(temp)
                    
    def prettify(self, elem):
        """Return a pretty-printed XML string for the Element.
        """
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        print reparsed.toprettyxml(indent="\t")   
