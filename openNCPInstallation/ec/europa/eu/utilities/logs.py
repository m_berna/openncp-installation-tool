'''
Created on Oct 23, 2015

@author: berna
'''
import platform


class Logging(object):
    '''
    classdocs
    '''


    def __init__(self, isPrint):
        self.isprint = isPrint
    
    
    #Check if minimum machine requirement are supported
    def machineRequirements(self):
        if self.isprint:
            print "*** Machine Specifications ***"
            print "You a running a %s machine, release %s, version %s " % (platform.system(), platform.release(), platform.version())
            print "Your architecture is %s and you're currently connected to the network: %s" %(platform.machine(), platform.node())
            print "You're running Python %s" % platform.python_version()
            raw_input("Press [ENTER] toMake sure that the user running the application has rights to write in the folders where you want to install")
    
    
    #===============================================================================
    #     2 GHz Xeon Processor or equivalent
    #     20 GB Storage
    #     4 GB Memory
    # Suggested Software Requirements
    # Linux or Microsoft Windows Operating System
    # Oracle Java SE Development Kit 7 jdk7u21 or earlier (refer to page comments)
    # Apache Tomcat 6.0.x or 7.0.x
    # Relational Database (tested with MySQL, Postgres, Oracle)
    # Openswan 2.6.x for IPsec (needed in epSOS, but not part of OpenNCP as such)
#===============================================================================    