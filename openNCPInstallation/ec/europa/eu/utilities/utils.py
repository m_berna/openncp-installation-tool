'''
Created on Oct 30, 2015

@author: root
'''
from os.path import os
import sys
import urllib2


class utl(object):
    '''
    classdocs
    '''


    def __init__(self, config, xml):
        self.config = config
        self.xml= xml
        
    def installApps(self, prj, ext, out_name, metadata_path, install_path): 
        concreteVersion = self.xml.getMavenVersion(os.path.join(metadata_path, "maven-metadata.xml"), self.config["version"])
        war = "%s-%s.%s" % (prj, concreteVersion, ext)
        response = urllib2.urlopen(os.path.join(metadata_path, concreteVersion , war))
        with open((os.path.join(install_path, "%s.%s" % (out_name, ext))), 'w') as war_file:
            print "====> Installing %s version %s in %s" %(prj, concreteVersion, install_path)
            war_file.write(response.read()) 
        
    def isSudo(self):
        if os.getuid() != 0:
            print "This program is not run as sudo or elevated this it will not work"
            sys.exit(-1)