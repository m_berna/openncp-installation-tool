'''
Created on Nov 4, 2015

@author: berna
'''
import ConfigParser
from os.path import os


class FakeSecHead(object):
    def __init__(self, fp):
        self.fp = fp
        self.asection = 'dummy'
        self.sechead = '[%s]\n' % self.asection
        
    def readline(self):
        if self.sechead:
            try: 
                return self.sechead
            finally: 
                self.sechead = None
        else: 
            return self.fp.readline()
        
        

class Helper(object):
    def __init__(self, config):
        self.config = config

    def portal_ext(self, path):
        portal_ext = ConfigParser.RawConfigParser()
        portal_ext.add_section('MySQL')
        portal_ext.set('MySQL', 'jdbc.default.driverClassName', 'com.mysql.jdbc.Driver')
        portal_ext.set('MySQL', 'jdbc.default.url', 'jdbc:mysql://localhost/lportal?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false ')
        portal_ext.set('MySQL', 'jdbc.default.username', self.config["username"])
        portal_ext.set('MySQL', 'jdbc.default.password', self.config["password"])
    
        portal_ext.add_section('EPSOS ROLES')
        portal_ext.set('EPSOS ROLES', 'system.roles', 'Doctor,Pharmacist,Patient,Nurse')
        portal_ext.set('EPSOS ROLES', 'system.roles.Doctor.description', 'Doctor Role')
        portal_ext.set('EPSOS ROLES', 'system.roles.Pharmacist.description', 'Pharmacist Role')
        portal_ext.set('EPSOS ROLES', 'system.roles.Patient.description', 'Patient Role')
        portal_ext.set('EPSOS ROLES', 'system.roles.Nurse.description', 'Nurse role')
        
        with open(os.path.join(path, "portal-ext.properties"), 'wb') as configfile:
            portal_ext.write(configfile)
    
    def tsam_properties_update(self):
        tsam_properties= ConfigParser.RawConfigParser()
        tsam_properties.read(os.path.join(self.config['EPSOS_PROPS_PATH'], "tsam.properties"))
        tsam_properties.add_section('OpenNCP')
        tsam_properties.set('OpenNCP','ltr.db.url','jdbc:mysql://%s:3306/database?useUnicode=true&characterEncoding=UTF-8' % self.config['databasehost'])
        tsam_properties.set('OpenNCP','ltr.db.user',self.config["username"])
        tsam_properties.set('OpenNCP','ltr.db.password',self.config["password"])
        tsam_properties.set('OpenNCP','ltr.db.driverClass','com.mysql.jdbc.Driver')
        
        with open(os.path.join(self.config['EPSOS_PROPS_PATH'], "tsam.properties"), 'wb') as configfile:
            tsam_properties.write(configfile)
            
    def tsam_properties(self):
        tsam_properties = ConfigParser.RawConfigParser()
        tsam_properties.add_section('Language')
        tsam_properties.set('Language', 'translationLanguage', self.config["language"])
        tsam_properties.set('Language', 'transcodingLanguage', 'en')
        tsam_properties.add_section('Hibernate')
        tsam_properties.set('Hibernate', 'ltr.hibernate.hibernate.dialect', self.config["hibernate.dialect"])
        tsam_properties.set('Hibernate', 'ltr.hibernate.connection.datasource', 'java:comp/env/jdbc/TSAM')
        with open(os.path.join(self.config['EPSOS_PROPS_PATH'], "tsam.properties"), 'wb') as configfile:
                tsam_properties.write(configfile)        
            
            
    def tsam_exp_properties(self, tsam_home):
        tsam_exp_properties = ConfigParser.RawConfigParser()
        tsam_exp_properties.add_section('Settings')
        tsam_exp_properties.set('Settings', 'database.class.name', 'com.mysql.jdbc.Driver')
        tsam_exp_properties.set('Settings', 'database.url', 'jdbc:mysql://localhost:3306/ltrdb')
        tsam_exp_properties.set('Settings', 'database.name', 'ltrdb')
        tsam_exp_properties.set('Settings', 'database.username', self.config['username'])
        tsam_exp_properties.set('Settings', 'database.password', self.config['password'])
        with open(os.path.join(tsam_home, "settings.properties"), 'wb') as configfile:
                tsam_exp_properties.write(configfile)        
            
            
    def  atna_properties(self, atna_home):
        atna_properties = ConfigParser.ConfigParser()
        atna_properties.readfp(FakeSecHead(open(os.path.join(atna_home, 'openatna.properties'))))
        atna_properties.set('dummy', 'db.username', self.config['atna_user'])
        atna_properties.set('dummy','db.password', self.config['atna_password'])
        atna_properties.set('dummy', 'ihe.actors.dir', atna_home)
        
        with open(os.path.join(atna_home, 'openatna.properties'), 'wb') as configfile:
                atna_properties.write(configfile)        
            
            